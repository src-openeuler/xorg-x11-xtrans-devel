Name:    xorg-x11-xtrans-devel
Version: 1.5.0
Release: 1
Summary: X.Org X11 developmental X transport library
License: MIT
URL:     http://www.x.org
Source0: https://www.x.org/archive/individual/lib/xtrans-%{version}.tar.gz

BuildArch: noarch
BuildRequires: pkgconfig
BuildRequires: xorg-x11-util-macros

Patch1: xtrans-1.0.3-avoid-gethostname.patch


%description
xtrans is a library of code that is shared among various X packages to
handle network protocol transport in a modular fashion, allowing a
single place to add new transport types. This is X.Org X11 developmental 
X transport library.

%prep
%autosetup -n xtrans-%{version} -p1 

%build
%configure --libdir=%{_datadir} --docdir=%{_pkgdocdir}

%install
%make_install INSTALL="install -p"
install -pm 644 AUTHORS ChangeLog COPYING README.md $RPM_BUILD_ROOT%{_pkgdocdir}

%check

%pre

%preun

%post

%postun

%files
%{_pkgdocdir}
%dir %{_includedir}/X11
%dir %{_includedir}/X11/Xtrans
%{_includedir}/X11/Xtrans/*
%{_datadir}/aclocal/xtrans.m4
%{_datadir}/pkgconfig/xtrans.pc

%changelog
* Wed Oct 11 2023 wulei <wu_lei@hoperun.com> - 1.5.0-1
- Update to 1.5.0

* Mon Jan 9 2023 mengwenhua <mengwenhua@xfusion.com> - 1.4.0-2
- Automatically disable inet6 transport if ipv6 is disabled on machine

* Wed Jan 12 2022 yaoxin <yaoxin30@huawei.com> - 1.4.0-1
- Upgrade xorg-x11-xtrans-devel to 1.4.0

* Mon Dec  9 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3.5-8
- Package init
